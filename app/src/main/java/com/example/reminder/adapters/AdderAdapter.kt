package com.example.reminder.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.reminder.R

import androidx.databinding.DataBindingUtil
import com.example.reminder.databinding.ReminderTypeBinding
import com.example.reminder.model.Adder


class AdderAdapter(var adderList: ArrayList<Adder>, val onTypeListener: OnTypeListener ) :
    RecyclerView.Adapter<AdderAdapter.ReminderViewHolder>() {

    lateinit var context: Context
    private var selectedPosition : Int = 0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        context = parent.context
//        return ReminderViewHolder(inflater.inflate(R.layout.reminder_type, parent, false))
        val view = DataBindingUtil.inflate<ReminderTypeBinding>(inflater, R.layout.reminder_type, parent, false)
        return ReminderViewHolder(view)
    }

    override fun getItemCount() = adderList.size

    override fun onBindViewHolder(holder: ReminderViewHolder, position: Int) {

        holder.view.adder = adderList[position]

        if (selectedPosition == position) holder.view.allAdder.setBackgroundResource(adderList[position].color)
        else holder.view.allAdder.setBackgroundResource(R.color.colorPrimary)

        holder.view.allAdder.setOnClickListener {
            selectedPosition = position
            notifyDataSetChanged()
            onTypeListener.onTypeSelector(adderList[position], position)
        }

//            val opt = BitmapFactory.Options()
//            opt.inMutable = true
//            val ribbon = BitmapFactory.decodeResource(context.resources, R.drawable.ribbon, opt)

//        else holder.view.setBackgroundResource(R.color.colorPrimary)
    }

    override fun onBindViewHolder(
        holder: ReminderViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
        holder.view.executePendingBindings()
    }

    class ReminderViewHolder(var view: ReminderTypeBinding) : RecyclerView.ViewHolder(view.root)


//    fun changeColor(myBitmap : Bitmap): Bitmap {
//        val allpixels = IntArray(myBitmap.getHeight() * myBitmap.getWidth())
//
//        myBitmap.getPixels(
//            allpixels,
//            0,
//            myBitmap.getWidth(),
//            0,
//            0,
//            myBitmap.getWidth(),
//            myBitmap.getHeight()
//        )
//
//        for (i in allpixels.indices) {
//            if (allpixels[i] == Color.rgb(237,28,36)) {
//                allpixels[i] = Color.rgb(0,0,255)
//            }
//        }
//
//        for (i in allpixels.indices) {
//            if (allpixels[i] == Color.rgb(195,195,195)) {
//                allpixels[i] = Color.rgb(0,0,255)
//            }
//        }
//
//        myBitmap.setPixels(
//            allpixels,
//            0,
//            myBitmap.getWidth(),
//            0,
//            0,
//            myBitmap.getWidth(),
//            myBitmap.getHeight()
//        )
//
//        return myBitmap
//    }
//
//    fun changeColor2(myBitmap : Bitmap, red : Int, green : Int, blue : Int): Bitmap {
//        val allpixels = IntArray(myBitmap.getHeight() * myBitmap.getWidth())
//
//        myBitmap.getPixels(
//            allpixels,
//            0,
//            myBitmap.getWidth(),
//            0,
//            0,
//            myBitmap.getWidth(),
//            myBitmap.getHeight()
//        )
//
//        for (i in allpixels.indices) {
//            if (allpixels[i] == Color.rgb(237,28,36)) {
//                allpixels[i] = Color.rgb(red,green,blue)
//            }
//        }
//
//        for (i in allpixels.indices) {
//            if (allpixels[i] == Color.rgb(195,195,195)) {
//                allpixels[i] = Color.rgb(red,green,blue)
//            }
//        }
//
//        myBitmap.setPixels(
//            allpixels,
//            0,
//            myBitmap.getWidth(),
//            0,
//            0,
//            myBitmap.getWidth(),
//            myBitmap.getHeight()
//        )
//
//        return myBitmap
//    }

    fun updateAdapter(adderList: ArrayList<Adder>) {
        this.adderList = adderList
        notifyDataSetChanged()
    }

    interface OnTypeListener{
        fun onTypeSelector(adder: Adder, position: Int)
    }


//    @BindingAdapter("android:icon")
//    fun loadImage(view : ImageView ,gradientDrawable: GradientDrawable){
//        view.setImageResource() = gradientDrawable
//    }


}