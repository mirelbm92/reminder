package com.example.reminder.utilities

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter

 class Utils {
    companion object {
        @JvmStatic @BindingAdapter("icon")
        fun loadImage(view : ImageView, @DrawableRes image : Int){
            view.setImageResource(image)
        }
    }
}