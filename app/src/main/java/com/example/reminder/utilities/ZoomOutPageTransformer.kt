package com.example.reminder.utilities

import android.view.View
import com.prabhat1707.verticalpager.VerticalPageTransformer

class ZoomOutPageTransformer : VerticalPageTransformer() {
    private val MIN_SCALE = 0.75f
    private val MIN_ALPHA = 0.5f


    override fun transformPage(view: View, position: Float) {
        if (position < -1.0f) {
            view.alpha = 0.0f

        } else {
            val scaleFactor: Float
            if (position <= 1) {
                scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position))
                view.translationY = position * view.height
                view.translationX = (-1 * view.width) * position

                view.scaleX = scaleFactor
                view.scaleY = scaleFactor
                view.alpha = (MIN_ALPHA + (((scaleFactor - MIN_SCALE) / (1 - MIN_SCALE)) * (1 - MIN_ALPHA)))

            }
            else {
                view.alpha = 0.0f
            }
        }
    }

}