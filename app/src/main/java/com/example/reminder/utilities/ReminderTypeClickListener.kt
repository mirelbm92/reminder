package com.example.reminder.utilities

import android.view.View

interface ReminderTypeClickListener {
    fun onReminderTypeClicked(view : View)
}