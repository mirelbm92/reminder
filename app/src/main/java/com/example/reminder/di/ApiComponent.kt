package com.example.reminder.di

import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject(services: CountriesServices)
}