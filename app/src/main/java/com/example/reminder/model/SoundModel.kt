package com.example.reminder.model

import android.net.Uri

data class SoundModel(
    val soundTitle : String?,
    val soundPath : Uri?,
    var soundSelected : Boolean?
)
