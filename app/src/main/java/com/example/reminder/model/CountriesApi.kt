package com.example.reminder.model

import dagger.Provides
import io.reactivex.Single
import retrofit2.http.GET


interface CountriesApi {

    @GET("users")
    fun getUsers(): Single<List<Country>>
}