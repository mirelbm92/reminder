package com.example.reminder.viewmodel

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.drawable.GradientDrawable
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.reminder.R
import com.example.reminder.model.Adder
import com.example.reminder.data.entity.Reminder
import com.example.reminder.data.ReminderDatabase
import com.example.reminder.model.SoundModel
import com.example.reminder.utilities.DateHelper
import com.example.reminder.utilities.Receiver
import com.example.reminder.view.MainActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AdderViewModel(application: Application) : BaseViewModel(application) {

    var adderList = MutableLiveData<ArrayList<Adder>>()
    var reminderDate = ObservableField<String>()
    var reminderTime = ObservableField<String>()
    var reminderSound = ObservableField<String>()
    var isTransparent = MutableLiveData<Boolean>()
    var dialogShow = MutableLiveData<Boolean>()
    var mp : MediaPlayer = MediaPlayer()
    var reminderType = ObservableField<String>()
    var soundsList : ArrayList<SoundModel> = ArrayList()

    fun refreshAdder() {
        val standard = Adder("1", "Standard", R.drawable.ic_add, gradient(R.color.darkGray), R.color.darkGray)
        val medicine = Adder("1", "Medication", R.drawable.pill_icon, gradient(R.color.vibrantRed), R.color.vibrantRed)
        val birthday = Adder("1", "Birthday", R.drawable.cake_icon, gradient(R.color.trueBlue), R.color.trueBlue)
        val call = Adder("1", "Call", R.drawable.call_icon, gradient(R.color.sandyYellow), R.color.sandyYellow)
        val shopping = Adder("1", "Shopping", R.drawable.shopping_icon, gradient(R.color.grassGreen), R.color.grassGreen)
        val event = Adder("1", "Event", R.drawable.ic_date, gradient(R.color.happyMagenta), R.color.happyMagenta)
        val sport = Adder("1", "Sport", R.drawable.fitenss_icon, gradient(R.color.black), R.color.black)
        val water = Adder("1", "Water", R.drawable.water_icon, gradient(R.color.simpleBlue), R.color.simpleBlue)
        val meal = Adder("1", "Meal", R.drawable.meal_icon, gradient(R.color.mealOrange), R.color.mealOrange)
        val adderLists = arrayListOf(standard, medicine, birthday, call, shopping, event, sport, water, meal)
        adderList.value = adderLists
        reminderTime.set("")
        reminderDate.set("Test Date")
        dialogShow.value = true
    }

    fun addReminder(title : String, date : String, time : String){
        if (title.equals("") || date.equals("") || time.equals("")) {

        }else {

            val reminder = Reminder(
                UUID.randomUUID().toString(),
                title.toString(),
                date.toString(),
                time.toString(),
                "repeat",
                "12/10/2020 - 13/10/2020",
                soundsList.filter { it.soundSelected == true }.get(0).soundPath.toString(),
                "repeat",
                adderList.value!![Integer.valueOf(reminderType.get()!!)].adderTitle,
                adderList.value!![Integer.valueOf(reminderType.get()!!)].adderIcon,
                adderList.value!![Integer.valueOf(reminderType.get()!!)].color,
                R.drawable.ic_repeat
            )
            dialogShow.value = false
            storeOneReminder(reminder)
        }
    }

    fun cancelReminderAddition(){
        goToNextActivity()
    }

    fun setTransparentBg(isTransparent : Boolean){
        this.isTransparent.value = isTransparent
    }

     fun goToNextActivity() {
        val intent = Intent(getApplication(), MainActivity::class.java)
         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
         intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
         getApplication<Application>().startActivity(intent)
    }

    private fun startAlarmManager(uniqueId: String, date: String, time: String, reminderId: Int) {
        setReminder(uniqueId, date, time, reminderId)
    }

    fun setReminder(uniqueId: String, date: String, time: String, reminderId: Int) {
        val alarmManager = getApplication<Application>().applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        val startTime = Calendar.getInstance()
        startTime.add(Calendar.SECOND, 10)
        Toast.makeText(this.getApplication(),"Id: " + reminderId, Toast.LENGTH_LONG).show()
        val intent = Intent(getApplication<Application>().applicationContext, Receiver::class.java)
//        val extra = Bundle()
//        extra.putString("id",uniqueId)
        intent.putExtra("extra",uniqueId)

        val pendingIntent = PendingIntent.getBroadcast(
            getApplication<Application>().applicationContext,
            reminderId,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
//        alarmManager!![AlarmManager.RTC, startTime.timeInMillis] = pendingIntent
        alarmManager!!.setExact(AlarmManager.RTC_WAKEUP, DateHelper.getCalendarDate(date, time).timeInMillis, pendingIntent)

//        val receiver = Receiver()
//        getApplication<Application>().registerReceiver( receiver, IntentFilter("receiver"))
//        alarmManager.set(
//            AlarmManager.ELAPSED_REALTIME_WAKEUP,
//            SystemClock.elapsedRealtime() + 10 * 1000, pendingIntent)
    }

    private fun storeOneReminder(reminder : Reminder){
        launch {
            val dao = ReminderDatabase(getApplication())
                .reminderDao()
            dao.insertOne(reminder)
            fetchOneFromDb(reminder.uniqueId)
        }
    }

    private fun fetchOneFromDb(reminderid : String) {
        launch(Dispatchers.IO) {
            val reminder = ReminderDatabase(
                getApplication()
            ).reminderDao().getReminder(reminderid)
            Log.e("TAG","Reminder: " + reminder)
            withContext(Dispatchers.Main) {
                startAlarmManager(reminder.uniqueId, reminder.reminderDate.toString(), reminder.reminderTime.toString(),reminder.reminderId)
                goToNextActivity()
            }
        }
    }

    fun setDate(date : Date){
        reminderDate.set(SimpleDateFormat.getDateInstance().format(date).toString())
    }

    fun setTime(hours : String, minutes : String){
        reminderTime.set("$hours:$minutes")
    }

    fun setTone(tone : String?){
        reminderSound.set(tone)
    }

    fun playTone(uri : Uri?){
//        RingtoneManager.getRingtone(getApplication(), uri).play()
        mp.release()
        mp = MediaPlayer.create(getApplication(),uri)
        mp.start()
        mp.setOnCompletionListener {
            mp.release()
        }
    }

    fun releaseMediaPlayer(){
        try {
            if (mp.isPlaying) mp.stop()
            mp.release()
        }catch (exception : Exception){
            Log.e("Tag","Caught exception : " + exception.message)
        }
    }

    fun ringToneList() : List<SoundModel> {
        val manager = RingtoneManager(getApplication<Application>())
        manager.setType(RingtoneManager.TYPE_RINGTONE)
        soundsList.clear()
        val cursor: Cursor = manager.cursor
        while (cursor.moveToNext()) {
            val uri: Uri = manager.getRingtoneUri(cursor.getPosition())
            val ringtoneName: String = cursor.getString(cursor.getColumnIndex("title"))
            soundsList.add(SoundModel(ringtoneName,uri,false))
        }
        soundsList.get(0).soundSelected = true
        for (soundModel : SoundModel in soundsList) {
            Log.e("TAG", "SoundModel :" + soundModel.soundSelected)
        }
        return soundsList
    }

    fun gradient(color: Int): GradientDrawable {
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            intArrayOf(
                ContextCompat.getColor(getApplication(), color),
                ContextCompat.getColor(getApplication(), color)
            )
        )
        gradientDrawable.cornerRadius = 10F
        return gradientDrawable
    }
}