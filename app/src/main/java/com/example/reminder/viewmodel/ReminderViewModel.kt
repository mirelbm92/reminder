package com.example.reminder.viewmodel

import android.app.Application
import android.graphics.drawable.GradientDrawable
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.example.reminder.data.ReminderDatabase
import com.example.reminder.data.entity.Reminder
import com.example.reminder.di.CountriesServices
import com.example.reminder.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception


class ReminderViewModel(application: Application) : BaseViewModel(application) {

    var reminderList = MutableLiveData<MutableList<Reminder>>()
    var loadingError = MutableLiveData<Boolean>()
    var loading = MutableLiveData<Boolean>()
    var mutableList: MutableList<Reminder> = mutableListOf()
    private val countriesServices = CountriesServices()
    private val disposable = CompositeDisposable()

    var mp : MediaPlayer = MediaPlayer()

    fun refresh() {
        fetchFromDb()
        fetchCountries()
    }

    private fun reminderRetrieve(reminderLists: MutableList<Reminder>) {
        reminderList.value = reminderLists
        loadingError.value = false
        loading.value = false
    }

//    fun setDate(date : Date){
//        reminderDate.set(SimpleDateFormat.getDateInstance().format(date).toString())
//    }
//
//    fun setTime(hours : String, minutes : String){
//        reminderTime.set("$hours:$minutes")
//    }
//
//    fun setTone(tone : String?){
//        reminderSound.set(tone)
//    }

//    private fun storeReminderLocally(reminder: Reminder) {
//        launch {
//            val dao = ReminderDatabase(getApplication()).reminderDao()
//            dao.insertOne(reminder)
//            fetchFromDb()
//        }
//    }

//    private fun storeOneReminder(reminder : Reminder){
//        launch {
//            val dao = ReminderDatabase(getApplication()).reminderDao()
//            dao.insertOne(reminder)
//            fetchFromDb()
//            fetchOneFromDb(reminder.uniqueId)
//        }
//    }

    private fun fetchFromDb() {
        loading.value = true
        launch(Dispatchers.IO) {
            mutableList = ReminderDatabase(
                getApplication()
            ).reminderDao().getAllReminders()
            withContext(Dispatchers.Main) {
                reminderRetrieve(mutableList.asReversed())
            }
        }
    }

    private fun fetchCountries() {
        loading.value = true

        disposable.add(
            countriesServices.getCountries()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Country>>() {
                    override fun onSuccess(value: List<Country>) {
                        Log.e("TAG","Value: " + value)
                    }

                    override fun onError(e: Throwable) {
                        Log.e("TAG","Value: " + e.message)
                    }
                    
                })
        )
    }

//    private fun fetchOneFromDb(reminderid : String) {
//        launch(Dispatchers.IO) {
//            val reminder = ReminderDatabase(getApplication()).reminderDao().getReminder(reminderid)
//            Log.e("TAG","Reminder: " + reminder)
//            withContext(Dispatchers.Main) {
//                startAlarmManager(reminder.uniqueId, reminder.reminderDate.toString(), reminder.reminderTime.toString(),reminder.reminderId)
//            }
//        }
//    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun deleteAll() {
        launch(Dispatchers.IO) {
            val dao = ReminderDatabase(getApplication())
                .reminderDao()
            dao.deleteAll()
        }
    }

//    fun onAddReminder(title : Editable, date : Editable, time : Editable, mode : Editable){
//        val reminder = Reminder(
//            UUID.randomUUID().toString(),
//            title.toString(),
//            date.toString(),
//            time.toString(),
//            "repeat",
//            "12/10/2020 - 13/10/2020",
//            soundsList.filter { it.soundSelected == true }.get(0).soundPath.toString(),
//            "repeate",
//            adderList.value!![Integer.valueOf(reminderType.get()!!)].adderTitle,
//            adderList.value!![Integer.valueOf(reminderType.get()!!)].adderIcon,
//            adderList.value!![Integer.valueOf(reminderType.get()!!)].color,
//            R.drawable.ic_repeat
//        )
//        storeOneReminder(reminder)
//        dialogShow.value = false
//        deleteAll()
//    }

//    private fun startAlarmManager(uniqueId: String, date: String, time: String, reminderId: Int) {
//        setReminder(uniqueId, date, time, reminderId)
//    }

//    fun refreshAdder() {
//        val standard = Adder("1", "Standard", R.drawable.ic_add, gradient(R.color.darkGray), R.color.darkGray)
//        val medicine = Adder("1", "Medication", R.drawable.pill_icon, gradient(R.color.vibrantRed), R.color.vibrantRed)
//        val birthday = Adder("1", "Birthday", R.drawable.cake_icon, gradient(R.color.trueBlue), R.color.trueBlue)
//        val call = Adder("1", "Call", R.drawable.call_icon, gradient(R.color.sandyYellow), R.color.sandyYellow)
//        val shopping = Adder("1", "Shopping", R.drawable.shopping_icon, gradient(R.color.grassGreen), R.color.grassGreen)
//        val event = Adder("1", "Event", R.drawable.ic_date, gradient(R.color.happyMagenta), R.color.happyMagenta)
//        val sport = Adder("1", "Sport", R.drawable.fitenss_icon, gradient(R.color.black), R.color.black)
//        val water = Adder("1", "Water", R.drawable.water_icon, gradient(R.color.simpleBlue), R.color.simpleBlue)
//        val meal = Adder("1", "Meal", R.drawable.meal_icon, gradient(R.color.mealOrange), R.color.mealOrange)
//        val adderLists = arrayListOf(standard, medicine, birthday, call, shopping, event, sport, water, meal)
//        adderList.value = adderLists
//        reminderTime.set("")
//        reminderDate.set("")
//        dialogShow.value = true
//    }

    fun gradient(color: Int): GradientDrawable {
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            intArrayOf(
                ContextCompat.getColor(getApplication(), color),
                ContextCompat.getColor(getApplication(), color)
            )
        )
        gradientDrawable.cornerRadius = 10F
        return gradientDrawable
    }

//    fun setReminder(uniqueId: String, date: String, time: String, reminderId: Int) {
//        val alarmManager = getApplication<Application>().applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
//        val startTime = Calendar.getInstance()
//        startTime.add(Calendar.SECOND, 10)
//        Toast.makeText(this.getApplication(),"Id: " + reminderId,Toast.LENGTH_LONG).show()
//        val intent = Intent(getApplication<Application>().applicationContext, Receiver::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//        val extra = Bundle()
//        extra.putString("id",uniqueId)
//        intent.putExtra("extra",extra)
//
//        val pendingIntent = PendingIntent.getBroadcast(
//            getApplication<Application>().applicationContext,
//            reminderId,
//            intent,
//            PendingIntent.FLAG_UPDATE_CURRENT
//        )
////        alarmManager!![AlarmManager.RTC, startTime.timeInMillis] = pendingIntent
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            alarmManager!!.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, DateHelper.getCalendarDate(date, time).timeInMillis, pendingIntent)
//        } else {
//            alarmManager!!.setExact(AlarmManager.RTC_WAKEUP, DateHelper.getCalendarDate(date, time).timeInMillis, pendingIntent)
//        }
//        val receiver = Receiver()
//        getApplication<Application>().registerReceiver( receiver, IntentFilter("receiver"))
//        alarmManager.set(
//            AlarmManager.ELAPSED_REALTIME_WAKEUP,
//            SystemClock.elapsedRealtime() + 10 * 1000, pendingIntent)
//    }

//     fun ringToneList() : List<SoundModel> {
//        val manager = RingtoneManager(getApplication<Application>())
//        manager.setType(RingtoneManager.TYPE_RINGTONE)
//        soundsList.clear()
//        val cursor: Cursor = manager.cursor
//        while (cursor.moveToNext()) {
//            val uri: Uri = manager.getRingtoneUri(cursor.getPosition())
//            val ringtoneName: String = cursor.getString(cursor.getColumnIndex("title"))
//            soundsList.add(SoundModel(ringtoneName,uri,false))
//        }
//         soundsList.get(0).soundSelected = true
//        return soundsList
//    }

    fun playTone(uri : Uri?){
//        RingtoneManager.getRingtone(getApplication(), uri).play()
        mp.release()
        mp = MediaPlayer.create(getApplication(),uri)
        mp.start()
        mp.setOnCompletionListener {
            mp.release()
        }
    }

    fun releaseMediaPlayer(){
        try {
            if (mp.isPlaying) mp.stop()
            mp.release()
        }catch (exception : Exception){
            Log.e("Tag","Caught exception : " + exception.message)
        }
    }
}