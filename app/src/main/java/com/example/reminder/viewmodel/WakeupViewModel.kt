package com.example.reminder.viewmodel

import android.app.Application
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.reminder.data.entity.Reminder
import com.example.reminder.data.ReminderDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception


class WakeupViewModel(application: Application) : BaseViewModel(application) {

    var reminder = MutableLiveData<Reminder>()
    var id : String? = ""
    var mp : MediaPlayer = MediaPlayer()

    fun initialization(id : String?){
        this.id = id
        fetchFromDb()
    }

    private fun fetchFromDb() {
        launch(Dispatchers.IO) {
           val reminder = ReminderDatabase(
               getApplication()
           ).reminderDao().getReminder(id)
            Log.e("TAG","Reminder: " + reminder)
            withContext(Dispatchers.Main) {
                reminderRetrieve(reminder)
            }
        }
    }

   fun reminderRetrieve(reminder: Reminder){
       this.reminder.value = reminder
   }

    fun playTone(){
//        RingtoneManager.getRingtone(getApplication(), uri).play()
        mp.release()
        mp = MediaPlayer.create(getApplication(), Uri.parse(reminder.value!!.reminderSong))
        mp.start()
        mp.setOnCompletionListener {
            mp.release()
        }
    }

    fun releaseMediaPlayer(){
        try {
            if (mp.isPlaying) mp.stop()
            mp.release()
        }catch (exception : Exception){
            Log.e("Tag","Caught exception : " + exception.message)
        }
    }

}
