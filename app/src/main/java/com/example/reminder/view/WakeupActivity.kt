package com.example.reminder.view

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.example.reminder.R
import com.example.reminder.adapters.FragmentsAdapter
import com.example.reminder.utilities.ZoomOutPageTransformer
import com.example.reminder.view.fragments.ClosingFragment
import com.example.reminder.view.fragments.WakeupFragment
import com.example.reminder.viewmodel.WakeupViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.wakeup_activity.*
import javax.inject.Inject


class WakeupActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    lateinit var viewModel : WakeupViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wakeup_activity)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        }
        viewModel = ViewModelProviders.of(this)[WakeupViewModel::class.java]
        viewModel.initialization(this.intent.getStringExtra("extra"))
        setupViewPager()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        viewModel.releaseMediaPlayer()
    }



    private fun setupViewPager() {
        val adapter = FragmentsAdapter(supportFragmentManager)
        adapter.addFragment(WakeupFragment(), "Wakeup")
        adapter.addFragment(ClosingFragment(), "Close")

//        adapter.addFragment(SettingsFragment(), "Settings")
        vertical_view_pager.offscreenPageLimit = 0
        vertical_view_pager.adapter = adapter
        vertical_view_pager.setPageTransformer(false, ZoomOutPageTransformer())
        vertical_view_pager.addOnPageChangeListener(this)
    }

    fun gradient(color: Int): GradientDrawable {
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            intArrayOf(
                ContextCompat.getColor(getApplication(), color),
                ContextCompat.getColor(getApplication(), color)
            )
        )
        return gradientDrawable
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        viewModel.releaseMediaPlayer()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

}
