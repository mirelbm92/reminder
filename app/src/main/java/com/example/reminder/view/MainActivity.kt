package com.example.reminder.view

import android.database.Cursor
import android.media.RingtoneManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.reminder.R
import com.example.reminder.adapters.FragmentsAdapter
import com.example.reminder.view.fragments.AlarmFragment
import com.example.reminder.view.fragments.ReminderFragment
import com.example.reminder.view.fragments.SettingsFragment
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewPager()

    }

    private fun setupViewPager() {
        val adapter = FragmentsAdapter(supportFragmentManager)
        adapter.addFragment(ReminderFragment(), "Reminder")
        adapter.addFragment(AlarmFragment(), "Alarms")
        adapter.addFragment(AlarmFragment(), "Stopwatch")
        adapter.addFragment(SettingsFragment(), "Timer")
        viewPager.offscreenPageLimit = 1
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)
    }

    fun floatingBackground(variable : Boolean){
        if (variable) floating_ground.visibility = View.VISIBLE
        else floating_ground.visibility = View.GONE
    }

}
