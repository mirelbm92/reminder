package com.example.reminder.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.reminder.R

/**
 * A simple [Fragment] subclass.
 */
class ClosingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        Toast.makeText(this.context,"Something",Toast.LENGTH_LONG).show()
        return inflater.inflate(R.layout.fragment_closing, container, false)
    }

}
