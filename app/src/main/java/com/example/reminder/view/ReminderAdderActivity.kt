package com.example.reminder.view

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reminder.R
import com.example.reminder.adapters.AdderAdapter
import com.example.reminder.model.Adder
import com.example.reminder.view.fragments.adderfragments.StandardAdderFragment
import com.example.reminder.viewmodel.AdderViewModel
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_reminder_adder.*


class ReminderAdderActivity : AppCompatActivity(), AdderAdapter.OnTypeListener {


    private lateinit var viewModel: AdderViewModel
    private val adderAdapter = AdderAdapter(arrayListOf(), this)
    lateinit var adder : Adder
    lateinit var fragmentManager : FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reminder_adder)

        viewModel = ViewModelProviders.of(this) [AdderViewModel::class.java]
        recycler_type.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = adderAdapter }
        observeAdderViewModelList()
        observeTransparencyChanges()
        fragmentManager = supportFragmentManager
        replaceFragment(StandardAdderFragment())
        viewModel.reminderType.set("0")
        transparent_bg.setOnClickListener{
            viewModel.setTransparentBg(false)
        }
    }

    override fun onBackPressed() {
    }

    fun replaceFragment(fragment : Fragment){
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun observeAdderViewModelList() {
        viewModel.adderList.observe(this, Observer { adderList ->
            adderList?.let {
                adderAdapter.updateAdapter(adderList)
            }
        })
    }

    fun observeTransparencyChanges() {
        viewModel.isTransparent.observe(this, Observer { isTransparent ->
            isTransparent?.let {
                if (isTransparent) transparent_bg.visibility = View.VISIBLE
                else transparent_bg.visibility = View.GONE
            }
        })
    }

    override fun onTypeSelector(adder: Adder, position: Int) {
        this.adder = adder
        viewModel.reminderType.set(position.toString())
        recycler_type.scrollToPosition(position)
    }

    private fun setTextInputLayoutHintColor(textInputLayout: TextInputLayout, context: Context?, @ColorRes colorIdRes: Int) {
        textInputLayout.defaultHintTextColor = ColorStateList.valueOf(ContextCompat.getColor(context!!, colorIdRes))
    }
}
