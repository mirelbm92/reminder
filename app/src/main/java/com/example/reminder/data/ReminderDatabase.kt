package com.example.reminder.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.reminder.data.entity.Reminder
import com.example.reminder.data.dao.ReminderDao

@Database(entities = arrayOf(Reminder::class),version = 2)
abstract class ReminderDatabase : RoomDatabase() {
    abstract fun reminderDao(): ReminderDao

    companion object{
        @Volatile private var instance: ReminderDatabase? = null
        private val LOCK = Any()
        val DATABASE_NAME = "reminder_db"

        operator fun invoke(context : Context) = instance
            ?: synchronized(LOCK){
            instance
                ?: buildDatabase(
                    context
                ).also{
                instance = it
            }
        }

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL(
                    "ALTER TABLE MyCardModel "
                            + " ADD COLUMN unique_identifier TEXT"
                )
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context.applicationContext, ReminderDatabase::class.java, "reminderdatabase").addMigrations(
            MIGRATION_1_2
        ).build()
    }



}